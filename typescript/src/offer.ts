import { IModel, Photography, VideoRecording, WeddingSession, BlurayPackage, TwoDayEvent } from "./models";
import { ServiceType } from ".";
import { IDiscount, PhotographyVideoRecordingDiscount, WeddingSessionDiscount } from "./discounts";

export class Offer {
    private ORIGINAL = true;
    protected models: IModel[] = null;
    protected discounts: IDiscount[] = null;

    constructor(protected services: ServiceType[], protected year: number) { 
        this.services = this.getUniqServices(services);        
    }

    calculateBestPrices() {
        this.initModel();

        let basePriceMaps: {id: string, price: number}[] = [];
        let basePrice: number = 0;
        this.models.forEach(model => {
            let price = model.getBasePrice(this.year);
            basePrice += price;
            basePriceMaps.push({id: model.getServiceName(), price: price});
        });

        let finalPrice: number = 0;
        /*  
            I sent a question for this exercise, but a bit too late and did not receive any feedback ... so I prepared two options :)
            With "ORIGINAL = true" - I assume that the original unit test should pass all the tests because they are correct and I have a misunderstanding
            one of the requirements.

            On the other hand, "ORIGINAL = false" is the second option (better from my point of view).
            Based on the requirement "Any discounts should never be applied twice - greater discount wins" - I thought that only one best
            from the list can be applied. In this case, some of the original tests will fail, becouse there is a logical bug. I added an additional two tests
            verify my point of view (calcularePrice.myTestOne and calcularePrice.myTestTwo are commented at the end of the test file).
        */
        if (this.ORIGINAL === true) {
            finalPrice = 0;
            let used: ServiceType[] = [];
            this.discounts.forEach(disc => {
                let price: number | null = disc.getDiscountPrice(this.year, this.services);
                if (price !== null) {
                    finalPrice += price;
                    used = used.concat(disc.getServiceNames());
                }       
            });
    
            let notUsed: ServiceType[] = this.services.filter(item => used.indexOf(item) < 0);
            notUsed.forEach(nu => {
                basePriceMaps.forEach(item => {
                    if (nu === item.id)
                        finalPrice += item.price;
                });
            });
        }
        else {
            finalPrice = basePrice;
            this.discounts.forEach(disc => {
                let used: ServiceType[] = [];
                let price: number | null = disc.getDiscountPrice(this.year, this.services);
                if (price !== null) 
                    used = used.concat(disc.getServiceNames());
                else 
                    price = 0;
    
                let notUsed = this.services.filter(item => used.indexOf(item) < 0);
                notUsed.forEach(nu => {
                    basePriceMaps.forEach(item => {
                        if (nu === item.id)
                        price += item.price;
                    });
                });
    
                if(price < finalPrice)
                    finalPrice = price;
            });
        }        
        
        return {basePrice, finalPrice};
    }

    private initModel() {
        if (this.models !== null)
            return;

        this.models = [];
        this.services.forEach(s => {
            switch(s) { 
                case "Photography": { 
                   this.models.push(new Photography());
                   break; 
                } 
                case "VideoRecording": { 
                    this.models.push(new VideoRecording());
                   break; 
                }
                case "WeddingSession": { 
                    this.models.push(new WeddingSession());
                   break; 
                }
                case "BlurayPackage": { 
                    this.models.push(new BlurayPackage());
                   break; 
                }
                case "TwoDayEvent": { 
                    this.models.push(new TwoDayEvent());
                   break; 
                }
                default: {
                   break; 
                } 
             }
        });

        this.discounts = [];
        this.discounts.push(new PhotographyVideoRecordingDiscount());
        this.discounts.push(new WeddingSessionDiscount());
    }

    private getUniqServices(items: ServiceType[]): ServiceType[] {
        return Array.from(new Set(items))
    }
}