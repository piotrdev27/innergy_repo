import { ServiceType } from ".";

export interface IDiscount {
    getDiscountPrice(year: number, availableServices: ServiceType[]): number | null;
    getServiceNames(): ServiceType[];
}

export class PhotographyVideoRecordingDiscount implements IDiscount {
    getDiscountPrice(year: number, availableServices: ServiceType[]): number | null {
        if (availableServices.indexOf("Photography") > -1 && availableServices.indexOf("VideoRecording") > -1) {
            if (year === 2020) return 2200;
            else if (year === 2021) return 2300;
            else if (year === 2022) return 2500;
        }

        return null;
    }
    getServiceNames(): ServiceType[] {
        return ["Photography", "VideoRecording"];
    }
}

export class WeddingSessionDiscount implements IDiscount {
    getDiscountPrice(year: number, availableServices: ServiceType[]): number | null {
        if (availableServices.indexOf("WeddingSession") < 0)
            return null;

        let photographyIndex: number = availableServices.indexOf("Photography");
        let videoRecordingIndex: number = availableServices.indexOf("VideoRecording");

        if (photographyIndex > -1 && year == 2022)
            return 0;

        if (photographyIndex > -1 || videoRecordingIndex > -1) {
            if (year === 2020) return 300;
            else if (year === 2021) return 300;
            else if (year === 2022) return 300;
        }

        return null;
    }
    getServiceNames(): ServiceType[] {
        return ["WeddingSession"];
    }
}