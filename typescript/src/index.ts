import { Offer } from "./offer";

export type ServiceYear = 2020 | 2021 | 2022;
export type ServiceType = "Photography" | "VideoRecording" | "BlurayPackage" | "TwoDayEvent" | "WeddingSession";

export const updateSelectedServices = (
    previouslySelectedServices: ServiceType[],
    action: { type: "Select" | "Deselect"; service: ServiceType }
) => {
    let service = action.service;
    let index = previouslySelectedServices.indexOf(service);

    if (action.type === "Select" && index < 0) 
        previouslySelectedServices.push(service);
    else if (action.type === "Deselect" && index > -1) 
        previouslySelectedServices.splice(index, 1);
    

    index = previouslySelectedServices.indexOf("BlurayPackage");
    if (index > -1 && previouslySelectedServices.indexOf("VideoRecording") < 0)
        previouslySelectedServices.splice(index, 1);

    index = previouslySelectedServices.indexOf("TwoDayEvent");
    if (index > -1 && previouslySelectedServices.indexOf("Photography") < 0 && previouslySelectedServices.indexOf("VideoRecording") < 0)
        previouslySelectedServices.splice(index, 1);

    return previouslySelectedServices;
};

export const calculatePrice = (selectedServices: ServiceType[], selectedYear: ServiceYear) => { 
    let offer: Offer = new Offer(selectedServices, selectedYear);
    let bestPrices = offer.calculateBestPrices();

    return ({ basePrice: bestPrices.basePrice, finalPrice: bestPrices.finalPrice });
}