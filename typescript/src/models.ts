import { ServiceType } from ".";

export interface IModel {
    getBasePrice(year: number): number;
    getServiceName(): ServiceType;
}


export class Photography implements IModel {
    getBasePrice(year: number): number {
        if (year === 2020) return 1700;
        else if (year === 2021) return 1800;
        else if (year === 2022) return 1900;
        return 0;
    }    
    getServiceName(): ServiceType {
        return "Photography";
    }
}

export class VideoRecording implements IModel {
    getBasePrice(year: number): number {
        if (year === 2020) return 1700;
        else if (year === 2021) return 1800;
        else if (year === 2022) return 1900;
        return 0;
    }
    getServiceName(): ServiceType {
        return "VideoRecording";
    }
}

export class WeddingSession implements IModel {
    getBasePrice(year: number): number {        
        return 600;
    }
    getServiceName(): ServiceType {
        return "WeddingSession";
    }
}

export class BlurayPackage implements IModel {
    getBasePrice(year: number): number {        
        return 300;
    }
    getServiceName(): ServiceType {
        return "BlurayPackage";
    }
}

export class TwoDayEvent implements IModel {
    getBasePrice(year: number): number {        
        return 400;
    }
    getServiceName(): ServiceType {
        return "TwoDayEvent";
    }
}